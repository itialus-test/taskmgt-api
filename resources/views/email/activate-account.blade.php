
<!DOCTYPE HTML
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office">

<head>

<!--[if gte mso 9]>
<xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml>
<![endif]-->

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="x-apple-disable-message-reformatting">
<!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--<![endif]-->

<!-- Your title goes here -->
<title>Thank you for subscribing. Please activate your account.</title>
<!-- End title -->

<!-- Start stylesheet -->
<style type="text/css">
    a,
    a[href],
    a:hover,
    a:link,
    a:visited {
        /* This is the link colour */
        text-decoration: none !important;
        color: #0000EE;
    }

    .link {
        text-decoration: underline !important;
    }

    p,
    p:visited {
        /* Fallback paragraph style */
        font-size: 15px;
        line-height: 24px;
        font-family: 'Helvetica', Arial, sans-serif;
        font-weight: 300;
        text-decoration: none;
        color: #000000;
    }

    h1 {
        /* Fallback heading style */
        font-size: 22px;
        line-height: 24px;
        font-family: 'Helvetica', Arial, sans-serif;
        font-weight: normal;
        text-decoration: none;
        color: #000000;
    }

    .ExternalClass p,
    .ExternalClass span,
    .ExternalClass font,
    .ExternalClass td {
        line-height: 100%;
    }

    .ExternalClass {
        width: 100%;
    }
</style>
<!-- End stylesheet -->

</head>

<!-- You can change background colour here -->

<body style="text-align: center; margin: 0; padding-top: 10px; padding-bottom: 10px; padding-left: 0; padding-right: 0; -webkit-text-size-adjust: 100%;background-color: #f2f4f6; color: #000000" align="center">

    <!-- Fallback force center content -->
    <div style="text-align: left;">

        <!-- Start single column section -->

        Hi {{$data['first_name']}},
        <br/>
        <br/>
        Congratulations! You have been registered to Bizmate Weather App.
        <br/>
        <br/>
        Please activate your account, copy the activate code and click the link below to begin the process.
        <br/>
        <a href="{{$data['activation_url']}}">{{$data['activation_url']}}</a>
        <br/>
        <br/>
        Activate Code:
        <br/>
       <h1>{{$data['activation_code']}}</h1>
        Once activated, you will be able to login to the system.<br/>
        Thank you very much.
        <br/>
        <br/>
        <hr>
        This is a system generated email.

    </div>

</body>

</html>
