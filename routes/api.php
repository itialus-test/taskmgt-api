<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function () {

    //Unprotected Routes *********************************************************************//

    Route::post('auth/login',[AuthController::class,'login']);

    //Password Reset Routes for those forgotten passwords
    Route::post('auth/forgot-password',[AuthController::class,'forgotPassword']);
    Route::post('auth/reset-password',[AuthController::class,'resetPassword']);

    //Register an account
    Route::match(['get', 'post'], '/account/register', [AccountController::class,'register']);

    //Activate an account
    Route::match(['get', 'post'], '/account/activate', [AccountController::class,'activate']);

    //Protected Routes *************************************************************************//

    //Authentication Route
    Route::middleware('auth:api')->prefix('auth')->group(function () {
        Route::middleware('auth:api')->post('/logout',[AuthController::class,'logout']);
        Route::middleware('auth:api')->post('check-token-status',[AuthController::class,'checkTokenStatus']);
        Route::middleware('auth:api')->match(['get', 'post'],'change-password',[AuthController::class,'changePassword']);
    });

    //User Route
     Route::middleware('auth:api')->prefix('user')->group(function () {
        Route::match(['get', 'post'], '', [UserController::class,'index']);
        Route::match(['get', 'post'],'/show/{id}',[UserController::class,'show']);
        Route::match(['get', 'post'],'/show-profile',[UserController::class,'showProfile']);
        Route::match(['get', 'post'], '/store', [UserController::class,'store']);
        Route::match(['get', 'post', 'put'], '/update/{id}', [UserController::class,'update']);
        Route::match(['get', 'post', 'put'], '/update-profile', [UserController::class,'updateProfile']);
        Route::match(['get', 'delete'], '/delete/{id}', [UserController::class,'destroy']);
        Route::match(['get', 'post'], '/restore/{id}', [UserController::class,'undestroy']);
        Route::match(['get', 'post'], '/stats', [UserController::class,'stats']);
        Route::match(['get', 'post'], '/option-list', [UserController::class,'optionList']);
        });

    //Account Route
    Route::middleware('auth:api')->prefix('account')->group(function () {
        Route::match(['get', 'post'], '', [AccountController::class,'index']);
        Route::match(['get', 'post'],'/show/{id}',[AccountController::class,'show']);
        //Route::match(['get', 'post'], '/store', [AccountController::class,'store']);
        Route::match(['get', 'post', 'put'], '/update/{id}', [AccountController::class,'update']);
        Route::match(['get', 'delete'], '/delete/{id}', [AccountController::class,'destroy']);
        Route::match(['get', 'post'], '/restore/{id}', [AccountController::class,'undestroy']);
        Route::match(['get', 'post'], '/stats', [AccountController::class,'stats']);
        });

      //Task Route
      Route::middleware('auth:api')->prefix('task')->group(function () {
        Route::match(['get', 'post'], '', [TaskController::class,'index']);
        Route::match(['get', 'post'],'/show/{id}',[TaskController::class,'show']);
        Route::match(['get', 'post'], '/store', [TaskController::class,'store']);
        Route::match(['get', 'post', 'put'], '/update/{id}', [TaskController::class,'update']);
        Route::match(['get', 'delete'], '/delete/{id}', [TaskController::class,'destroy']);
        Route::match(['get', 'post'], '/restore/{id}', [TaskController::class,'undestroy']);
        Route::match(['get', 'post'], '/stats', [TaskController::class,'stats']);
        Route::match(['get', 'post'], '/option-list', [TaskController::class,'optionList']);
        });

});
