<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {

        $firstName = fake()->firstName();
        $lastName = fake()->lastName();

        return [
            'type' => fake()->numberBetween($min = 2, $max = 2),
            'first_name' =>$firstName,
            'last_name' => $lastName,
            'name' => $firstName.' '.$lastName,
            'email' => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'phone' => fake()->phoneNumber(),
            'address1' => fake()->streetAddress(),
            'address2' => fake()->address(),
            'city' => fake()->city(),
            'subnational_entity' => fake()->state(),
            'zip_postal_code' =>fake()->postcode(),
            'country_id' => fake()->numberBetween($min = 1, $max = 200),
            'role_id' => fake()->numberBetween($min = 1, $max = 4),
            'activation_code' => null,
            'account_status' => 1,
            'status' => 1,
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
