<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('type');
            $table->unsignedBigInteger('account_id')->default(0);
            $table->string('first_name',100);
            $table->string('last_name',100);
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone',50)->nullable();
            $table->string('address1',255)->nullable();
            $table->string('address2',255)->nullable();
            $table->string('city')->nullable();
            $table->string('subnational_entity')->nullable();
            $table->string('zip_postal_code',50)->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('role_id')->nullable();
            $table->string('picture',255)->nullable();
            $table->rememberToken();
            $table->boolean('status')->default(0);
            $table->boolean('account_status')->default(0);
            $table->string('activation_code',50)->nullable();
            $table->string('password_reset_code',50)->nullable();
            $table->timestamps();
            $table->softDeletes();

            //set Normal indexes
            $table->index('type');
            $table->index('account_id');
            $table->index('first_name');
            $table->index('last_name');
            $table->index('phone');
            $table->index('address1');
            $table->index('address2');
            $table->index('city');
            $table->index('subnational_entity');
            $table->index('zip_postal_code');
            $table->index('country_id');
            $table->index('role_id');
            $table->index('status');
            $table->index('account_status');
            $table->index('activation_code');
            $table->index('password_reset_code');
            $table->index('created_at');


        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
