<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        //create 4 deafult roles
        Role::factory()->create([
            'account_id' => 1,
            'role' => 'Super Admin',
            'description' => 'Super Admin has all access to all parts of the system',
        ]);

        Role::factory()->create([
            'account_id' => 1,
            'role' => 'Account Manager',
            'description' => 'Account Manager has access to the most part of the system in order to perform managerial task',
        ]);

        Role::factory()->create([
            'account_id' => 1,
            'role' => 'Regular User',
            'description' => 'Regular Employee access are primarily to handle day to day transactions.',
        ]);

        Role::factory()->create([
            'account_id' => 1,
            'role' => 'Reporter',
            'description' => 'Reporter access are primarily to handle report generation.',
        ]);
    }
}
