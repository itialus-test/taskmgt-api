<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        //Execute Seeds for User
        $this->call(UserSeeder::class);


        //Execute Seeds for Task
        $this->call(TaskSeeder::class);

        //if you want to seed Task Model only do this
        //php artisan db:seed --class=TaskSeeder

    }
}
