<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**

 */
final class Status extends Enum
{
    const NULL = null;
    const Inactive = 0;
    const Completed = 1;
    const Pending = 2;
    const OnProgress = 3;

}
