<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

final class FileName extends Enum
{
    const CSVFileFormat = ".csv";
    const EXCELFileFormat = ".xlsx";
    const PDFFileFormat = ".pdf";

    const EXCELFile ="XLSX";
    const CSVFile ="CSV";
    const PDFFile = "PDF";

    const UserExportFile ="user_export";
    const AccountExportFile = "account_export";

    const TaskExportFile = "task_export";
}
