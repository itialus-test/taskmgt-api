<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 */
final class AccountType extends Enum
{
    const Admin = 1;
    const Subscriber = 2;

}
