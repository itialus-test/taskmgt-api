<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class SystemMessage extends Enum
{
    const InvalidCredentials = "Authentication Fail. Invalid Credentials";
    const AuthLoginFailed="Authentication Failed. Invalid credentials";
    const AuthAccessSuspended="Authentication Failed. You're account is currently suspended. Please contact service provider.";
    const AuthLoginSuccess="Authentication Successful.";
    const AuthSendPasswordResetLink="A password reset link has been sent to your email. Check your email and complete the reset process.";
    const ResetPasswordFailed = "Reset Password Failed. No such request found.";
    const ChangePasswordFailed = "Change Password Failed. Your old password is incorrect.";
    const ResetPasswordSuccess = "Congratulations. Your new password has been set successfully.";
    const ChangePasswordSuccess = "Congratulations. Your password has been changed successfully.";
    const ApplicationError = "Application Error";
    const GeneralError = "General Error";
    const QueryError = "Query Error";
    const ValidationError="Validation Errors";

    //File Messages
    const FileFormatNotSupported = "File format not supported";

     //Common Messages
    const RejectionDetails="N1 transaction(s) successfully added But failed to add the following past transactions. Click Add button to add them one by one.";
    const NotFound=" Not Found";
    const AlreadyBeenUsed = " as it is already been used as referrence by other records";
    const StatsRetrieved = "Stats has been retrieved";
    const SystemLoginUrl = "https://beta-igo.talentbuilders.org/login";
    const FileReadyForDownload="File is ready for download";
    const FileDownloadInitiated="File download initiated";
    const AuthLogout="User has been logout. Goodbye.";
    const TokenNoLongerValid = "Token no longer valid";
    const TokenStillValid = "Token is still valid";
    const UnauthorizedAccess = "Unauthorize access.You don't have permission to access this route.";


    //User Messages
    const UserRecordCreated="New user record has been created";
    const UserRecordUpdated="User record has been updated";
    const UserRecordDeleted="User record has been deleted";
    const UserRecordRestored="User record has been restored";
    const UserRecordFound="User record found";
    const UserRecordRetrieved="User record has been retrieved";
    const UserNoRecordFound="No user record found";
    const UserID="User id ";
    const UserCanNotDelete="Can not delete user id ";
    const UserProfilePicUpdated="User Profile Pic has been updated.";
    const UserEmail="User Email ";
    const UserEmailAlreadyExist="Unable to process request. Email already exist. Please use another email.";

    //Task Messages
    const TaskRecordCreated="New task has been created";
    const TaskRecordUpdated="Task has been updated";
    const TaskRecordDeleted="Task has been deleted";
    const TaskRecordRestored="Task has been restored";
    const TaskRecordFound="Task found";
    const TaskRecordRetrieved="Task has been retrieved";
    const TaskNoRecordFound="No Task found";
    const TaskID="Task id ";
    const TaskCanNotDelete="Can not delete Task id ";

     //Account Messages
     const AccountRecordCreated="New account record has been created. Please check your email and confirm to activate your account. Thanks.";
     const AccountRecordUpdated="Account record has been updated";
     const AccountRecordDeleted="Account record has been deleted";
     const AccountRecordRestored="Account record has been restored";
     const AccountRecordFound="Account record found";
     const AccountRecordRetrieved="Account record has been retrieved";
     const AccountNoRecordFound="No account record found";
     const AccountID="Account id ";
     const AccountCanNotDelete="Can not delete account id ";
     const AccountAlreadyExist="Unable to create the account. The email already exist. Please use another email.";
     const AccountActivationFailed = "Activation Failed. No such request found.";
     const AccountHasBeenActivated = "Congratulations. Your account has been successfully activated.";

    //Mail Messages
    const MailSubjectActivateAccount="Welcome to Bizmate Weather App. Please activate your account.";
    const MailSubjectResetPassword="You Have Requested For Password Reset";
    const MailResetPasswordMailed="A reset password verification code has been sent to your email. Check your email get the verification code and complete your password reset. Thank you.";

}
