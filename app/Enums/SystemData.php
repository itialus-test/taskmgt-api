<?php declare(strict_types=1);

namespace App\Enums;

use BenSampo\Enum\Enum;

final class SystemData extends Enum
{
    const Data = 'data';
    const Message = 'message';
    const ActiveRecords = "active_records";
    const SoftDeletedRecords ="soft_deleted_records";
    const TotalRecords = "total_records";
    const Success = "success";
    const Error = "error";
    const RejectedData = "rejected_data";
    const RejectedMessage="rejected_message";
    const Description="description";
    const DownloadFileURL="download_file_url";
    const RawData="raw_data";
    const AccessToken = 'access_token';
    const TokenStatus='token_status';
    const ResultCount='result_count';
    const LastPage='last_page';

}
