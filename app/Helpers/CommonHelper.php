<?php

use App\Models\Country;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

if (!function_exists('burtTest')) {
    function burtTest(){
        echo "Hello this is just a helper test created by burt";
    }

}


if (!function_exists('getCountry')) {
    function getCountry($id){
        if($id===null)
        {
            return null;
        }else{
            return Country::find($id);
        }
    }

}

if (!function_exists('getRole')) {
    function getRole($id){
        return Role::find($id);
    }

}


if (!function_exists('GetSqlWithBindings')) {

    /**
     * description
     *
     * @param
     * @return
     */
 function GetSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}
?>
