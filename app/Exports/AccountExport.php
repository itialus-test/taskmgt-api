<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\Resources\ManufacturerResourceExport;

class AccountExport implements FromCollection, WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public $dataResult;

    public function __construct($dataResult)
    {
        $this->dataResult = $dataResult;
    }
    public function collection()
    {
        return ManufacturerResourceExport::collection($this->dataResult);
    }
    public function headings(): array
    {
        return [
            'ID',
            'First Name',
            'Middle Name',
            'Last Name',
            'Email',
            'Website',
            'Phone1',
            'Phone2',
            'Phone3',
            'Fax',
            'Address1',
            'Address2',
            'Zip/PostalCode',
            'City',
            'Subnational Entity',
            'Country',
            'Status',
            'tag',
            'Created at'
        ];
    }
}
