<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Http\Resources\TaskResourceExport;

class TaskExport implements FromCollection, WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public $dataResult;

    public function __construct($dataResult)
    {
        $this->dataResult = $dataResult;
    }
    public function collection()
    {
        return TaskResourceExport::collection($this->dataResult);
    }
    public function headings(): array
    {
        return [
            'Title',
            'ID',
            'Description',
            'Status',
            'User',
            'Created at'
        ];
    }
}
