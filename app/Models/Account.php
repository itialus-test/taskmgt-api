<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'website',
        'phone1',
        'phone2',
        'phone3',
        'fax',
        'address1',
        'address2',
        'zip_postal_code',
        'city',
        'subnational_entity',
        'country_id',
        'note',
        'tag',
        'status',
        'created_at'
    ];

    public $selectedFields=[
        'accounts.id',
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'website',
        'phone1',
        'phone2',
        'phone3',
        'fax',
        'address1',
        'address2',
        'zip_postal_code',
        'city',
        'subnational_entity',
        'country_id',
        'countries.country',
        'note',
        'status',
        'tag',
        'accounts.created_at'
         ];

    public function scopeFromCurrentAccount($query)
    {
        //always check that records belong to the account_id
        //of the currently authenticated user
        return $query->where('account_id','=',auth()->user()->account_id);
    }

    public function scopeCustomSearch($query)
    {
         return $query->select($this->selectedFields)
         ->leftjoin('countries', 'accounts.country_id', '=', 'countries.id');
    }

    public function scopeCustomCount($query)
    {
         return $query->selectraw('count(*) as count')
         ->leftjoin('countries', 'accounts.country_id', '=', 'countries.id');
    }

}
