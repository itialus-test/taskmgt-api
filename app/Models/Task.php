<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends Model
{
    use HasFactory, SoftDeletes;

    protected $dates = ['deleted_at'];
     /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'description',
        'user_id',
        'status'
    ];

    public $selectedFields=[
        'tasks.id',
        'tasks.title',
        'tasks.description',
        'tasks.status',
        'tasks.user_id',
        'users.first_name',
        'users.last_name',
        'tasks.created_at'
         ];

    public function scopeCustomSearch($query)
    {
        return $query->select($this->selectedFields)
        ->leftjoin('users', 'tasks.user_id', '=', 'users.id');
    }
    public function scopeCustomCount($query)
    {
        return $query->selectraw('count(*) as count');
    }
}
