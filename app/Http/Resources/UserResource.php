<?php

namespace App\Http\Resources;

use App\Enums\AccountType;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
       return[
        'id'=>$this->id,
        //'type'=>AccountType::getDescription($this->type),
        'account_id'=>$this->account_id,
        'first_name'=>$this->first_name,
        'last_name'=>$this->last_name,
        'name'=>$this->name,
        'email'=>$this->email,
        'phone'=>$this->phone,
        'address1'=>$this->address1,
        'address2'=>$this->address2,
        'city'=>$this->city,
        'subnational_entity'=>$this->subnational_entity,
        'zip_postal_code'=>$this->zip_postal_code,
        'country_id'=>$this->country_id,
        'country'=>$this->country,
        'role_id'=>$this->role_id,
        'role'=>$this->role,
        'picture'=>$this->picture,
        'created_at'=>$this->created_at->toDateTimeString(),
     ];
    }
}
