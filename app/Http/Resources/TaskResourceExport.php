<?php

namespace App\Http\Resources;

use App\Enums\Status;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResourceExport extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        //return parent::toArray($request);
        return[
            'title'=>$this->title,
            'id'=>$this->id,
            'description'=>$this->description,
            'status'=>Status::getDescription($this->status),
            'user_id'=>$this->user_id,
            'assign_to'=>$this->first_name.' '.$this->last_name,
            'created_at'=>$this->created_at->toDateTimeString(),
         ];
    }
}
