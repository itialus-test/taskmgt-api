<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;

use App\Exports\TaskExport;
use App\Http\Resources\TaskResource;
use App\Http\Requests\ViewTaskRequest;
use Illuminate\Database\QueryException;
use App\Enums\SystemMessage;
use App\Enums\HttpStatusCode;
use App\Enums\FileName;
use App\Enums\SystemData;
use Illuminate\Support\Facades\DB;

class TaskController extends Controller
{
    public function index(ViewTaskRequest $request)
    {

        //Validation is automatically done by
        //the ViewRequest class

        //extract page size
        $per_page = $request->input('per_page');

        $wildCard = ($request->filter_activatewildcard == 1) ? '%' : '';


        //Let's build our queries
        if($request->count_only){
            $dataQuery = Task::CustomCount();
        }else{
            $dataQuery = Task::CustomSearch();
        }

        //Check for Filters Begin -------------------------------------------------------//

        //Filter All COlumn
        if ($request->filter_allcolumn != '') {
            $dataQuery->where('id', 'LIKE', $wildCard . $request->filter_allcolumn . '%');
            $dataQuery->Orwhere('title', 'LIKE', $wildCard . $request->filter_allcolumn . '%');
            $dataQuery->Orwhere('description', 'LIKE', $wildCard . $request->filter_allcolumn . '%');
            $dataQuery->Orwhere('created_at', 'LIKE', $wildCard . $request->filter_allcolumn . '%');
        }

        //Filter assign_to
        if ($request->filter_assign_to != '') {
            $dataQuery->where(DB::Raw('concat(first_name, last_name)'), 'LIKE', $wildCard . $request->filter_assign_to . '%');
        }

        //Filter ID
        if ($request->filter_id != '') {
            $dataQuery->where('id', 'LIKE', $wildCard . $request->filter_id . '%');
        }

        //Filter Name
        if ($request->filter_title != '') {
            $dataQuery->where('title', 'LIKE', $wildCard . $request->filter_title . '%');
        }

        //Filter Description
        if ($request->filter_description != '') {
            $dataQuery->where('description', 'LIKE', $wildCard . $request->filter_description . '%');
        }

        //Filter Status
        if ($request->filter_status != '') {
            $dataQuery->where('status', 'LIKE', $wildCard . $request->filter_status . '%');
        }

        //Filter Created
        if ($request->filter_created_at != '') {
            $dataQuery->where('created_at', 'LIKE', $wildCard . $request->filter_created_at . '%');
        }

        //Check for Filters End -------------------------------------------------------//

        //Apply Sorting Mechanism Begin ----------------------------------------------//
        if(!$request->count_only){

            //Sort by ID
            if ($request->sort_id) {
                $dataQuery->orderBy('id', ($request->sort_id == 1) ? "asc" : "desc");
            }

            //Sort by Name
            if ($request->sort_title) {
                $dataQuery->orderBy('title', ($request->sort_title == 1) ? "asc" : "desc");
            }

            //Sort by Description
            if ($request->sort_description) {
                $dataQuery->orderBy('description', ($request->sort_description == 1) ? "asc" : "desc");
            }

            //Sort by status
            if ($request->sort_status) {
                $dataQuery->orderBy('status', ($request->sort_status == 1) ? "asc" : "desc");
            }

            //Sort by assign_to
            if ($request->sort_assign_to) {
                $dataQuery->orderBy(DB::raw('CONCAT(first_name,last_name)'), ($request->sort_assign_to == 1) ? "asc" : "desc");
            }

            //Sort by created_at
            if ($request->sort_created_at) {
                $dataQuery->orderBy('created_at', ($request->sort_created_at == 1) ? "asc" : "desc");
            }

        }

        //Apply Sorting Mechanism End -----------------------------------------==-----//


        try {

             if($request->count_only){
                $dataResult = $dataQuery->first();
              }else{
                $dataResult = $dataQuery->paginate($per_page);
              }

        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }

        //if the request is count_only then compute the last page
        //then automatically return the count results.
        if($request->count_only){
            if (intval($dataResult->getAttribute('count')) >1 || intval($request->per_page) >1)
            {
                $lastPage =intval(ceil( intval($dataResult->getAttribute('count')) / intval($request->per_page) ));
            }else{
                $lastPage = 1;
            }

            return response()->json(
                [
                    SystemData::ResultCount => $dataResult->getAttribute('count'),
                    SystemData::LastPage => $lastPage,
                    SystemData::Success => false
                ]);
        }

        //if the request is NOT count only then continue to perform the code below
        //Check if dataResult has record, if not throw message
        if ($dataResult->count() <= 0) {
            return response()->json(
                [
                    SystemData::Message => SystemMessage::TaskNoRecordFound,
                    SystemData::Success => false
                ],
                HttpStatusCode::ClientErrorNotFound
            );
        }

        //Check if this request is for EXPORT.
        //If export_to is NOT '' means YES then export the data to target format
        if ($request->export_to != '') {
            switch (strtoupper($request->export_to)) {
                case FileName::EXCELFile:
                    return (new TaskExport($dataResult))->download(FileName::TaskExportFile . FileName::EXCELFileFormat, \Maatwebsite\Excel\Excel::XLSX, null);
                    break;
                case FileName::CSVFile:
                    return (new TaskExport($dataResult))->download(FileName::TaskExportFile . FileName::CSVFileFormat, \Maatwebsite\Excel\Excel::CSV, null);
                    break;

                default:
                    return response()->json(
                        [
                            SystemData::Message => SystemMessage::FileFormatNotSupported,
                            SystemData::Success => false
                        ],
                        HttpStatusCode::ClientErrorBadRequest
                    );
            }
        }
        //dd($dataResult);

        return TaskResource::collection($dataResult)->additional([
            SystemData::Message => SystemMessage::TaskRecordRetrieved,
            SystemData::Success => true
        ]);
    }

    public function all()
    {
        //Show all records including those mark deleted
        $dataResult = Task::CustomSearch()->withTrashed()
                      ->simplepaginate(100);

        return TaskResource::collection($dataResult)->additional([
            SystemData::Message => SystemMessage::TaskRecordRetrieved,
            SystemData::Success => true
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */

    public function store(StoreTaskRequest $request)
    {

        //Validation is automatically handled
        //by StoreUserRequest

        //Pick Up only all those data from the request
        //that are needed to save
        $data = [
            "title" => $request->title,
            "description" => $request->description,
            "user_id" => $request->user_id,
            "status" => $request->status,
        ];

        try {
            //Execute and return the newly created record
            $category = Task::create($data);

            //return the resource with additional element
            return TaskResource::collection([$category])->additional([
                SystemData::Message => SystemMessage::TaskRecordCreated,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Display the specified resource.
     */

    public function show($id)
    {
        try {

            $category = Task::CustomSearch()->find($id);

            if (is_null($category)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::TaskID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //show found record as object
            return response()->json([
                SystemData::Data => TaskResource::make($category),
                SystemData::Message => SystemMessage::TaskRecordFound,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Update the specified resource in storage.
     */

    public function update(UpdateTaskRequest $request, $id)
    {
        //Validation is automatically handled
        //by the UpdateUserRequest

        //Pick Up only all those data from the request
        //that are needed to update
        $data = [
            "title" => $request->title,
            "description" => $request->description,
            "user_id" => $request->user_id,
            "status" => $request->status,
        ];

        //Find and return the found record
        try {
            $category = Task::CustomSearch()->find($id);

            if (is_null($category)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::TaskID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute update to update the record
            $category->update($data);
            $category = Task::CustomSearch()->find($id);

            //show found record as object
            return response()->json([
                SystemData::Data => TaskResource::make($category),
                SystemData::Message => SystemMessage::TaskRecordUpdated,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }

    /**
     * Remove the specified resource from storage.
     */

    public function destroy($id)
    {

        try {
            //Before delete make sure to check first if this client ID
            //is not yet been used as referrence by accounts record

            if (!is_null(null)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::TaskCanNotDelete . $id . SystemMessage::AlreadyBeenUsed,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

           $category = Task::CustomSearch()->find($id);

            if (is_null($category)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::TaskID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute Delete and return the deleted record
            $category->delete();


            //show deleted record as object
            return response()->json([
                SystemData::Data => TaskResource::make($category),
                SystemData::Message => SystemMessage::TaskRecordDeleted,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }


    public function undestroy($id)
    {
        try {

            $category = Task::CustomSearch()->onlyTrashed()->find($id);

            if (is_null($category)) {
                return response()->json(
                    [
                        SystemData::Message => SystemMessage::TaskID . $id . SystemMessage::NotFound,
                        SystemData::Success => false
                    ],
                    HttpStatusCode::ClientErrorNotFound
                );
            }

            //Execute Restore
            $category->restore();

            //show undeleted record as object
            return response()->json([
                SystemData::Data => TaskResource::make($category),
                SystemData::Message => SystemMessage::TaskRecordRestored,
                SystemData::Success => true
            ]);
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }


    public function stats()
    {
        try {

            $activeRecords = Task::CustomSearch()->count();
            $softDeletedRecords = Task::CustomSearch()->onlyTrashed()->count();
            $totalRecords = $activeRecords + $softDeletedRecords;

            //return the Resource with additional element
            return response()->json(
                [
                    SystemData::Data => [
                        SystemData::ActiveRecords => $activeRecords,
                        SystemData::SoftDeletedRecords => $softDeletedRecords,
                        SystemData::TotalRecords => $totalRecords
                    ],
                    SystemData::Message => SystemMessage::StatsRetrieved,
                    SystemData::Success => true
                ],
                HttpStatusCode::SuccessOK
            );
        } catch (QueryException $e) { // Catch all Query Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Exception $e) {     // Catch all General Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        } catch (\Error $e) {          // Catch all Php Errors
            return response()->json([SystemData::Message => $e->getMessage(), SystemData::Success => false], HttpStatusCode::ServerErrorInternalServerError);
        }
    }


}
